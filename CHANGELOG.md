# Changelog

## v3.5.5

* Flash fixed fonts in UI
* work with 1.13.0

## v3.5.4

* repo moved to gitlab