﻿[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_wgfm-radio&metric=alert_status)](https://sonarcloud.io/dashboard?id=wot-public-mods_wgfm-radio)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_wgfm-radio&metric=ncloc)](https://sonarcloud.io/dashboard?id=wot-public-mods_wgfm-radio)
[![Visits](https://gitlab.poliroid.ru/api/badge/wgfm-radio/visits)](https://gitlab.com/wot-public-mods/wgfm-radio)
[![Downloads](https://gitlab.poliroid.ru/api/badge/wgfm-radio/downloads)](https://gitlab.com/wot-public-mods/wgfm-radio/-/releases)
[![Donate](https://cdn.poliroid.ru/gitlab/donate.svg)](https://poliroid.ru/donate)

**WGFM Radio** This is a modification for the game "World Of Tanks" which allows you to listen WarGaming.FM radio directly ingame

### An example of what radio looks like
![An example of what radio looks like](https://static.poliroid.ru/wgfmRadio.jpg)

