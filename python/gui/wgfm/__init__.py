﻿
__author__ = "Andruschyshyn Andrey"
__copyright__ = "Copyright 2021, poliroid"
__credits__ = ["Andruschyshyn Andrey"]
__license__ = "LGPL-3.0-or-later"
__version__ = "3.5.5"
__maintainer__ = "Andruschyshyn Andrey"
__email__ = "poliroid@pm.me"
__status__ = "Production"

from gui.wgfm.data import *
from gui.wgfm.hooks import *
from gui.wgfm.events import g_eventsManager
from gui.wgfm.controllers import g_controllers
from gui.wgfm.views import *

__all__ = ('init', 'fini')

def init():
	g_controllers.init()

def fini():
	g_eventsManager.onAppFinish()
